#[cfg(target_os = "windows")]
extern crate winres;

fn main() {
    #[cfg(target_os = "windows")]
    windows_icon();
}

#[cfg(target_os = "windows")]
fn windows_icon() {
    let mut res = winres::WindowsResource::new();
    res.set_icon("src/assets/logo.ico"); 
    res.compile().unwrap();
}
