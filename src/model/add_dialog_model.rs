use std::path::{Path, PathBuf};

pub struct AddSetupModel {
    pub path: Box<Path>,
    pub next_files: Vec<PathBuf>,
    pub mode: TransferMode,
    pub drag_drop: bool
}

#[derive(Debug, Clone, Copy)]
pub enum TransferMode {
    Add,
    Move,
    Copy
}

