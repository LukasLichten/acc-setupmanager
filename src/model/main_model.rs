use std::{path::Path, cmp::Ordering};

use crate::backend::{crawler, renamer};

#[derive(Debug, Clone)]
pub struct Setup {
    pub path: Box<Path>,
    pub name: String,
    pub car_name: String,
    pub track: String,
    pub date: String
}

#[derive(Debug, Clone)]
pub struct MainModel {
    pub acc_settings_folder: Box<Path>,
    pub view_list: Vec<Setup>,
    pub unflitered_list: Vec<Setup>,
    pub text_filter: String,
    pub sort: Sort
}

#[derive(Debug, Clone, Copy)]
pub enum SortCategory {
    Name,
    Track,
    Car,
    Date
}

#[derive(Debug, Clone, Copy)]
pub enum Sort {
    Asc(SortCategory),
    Desc(SortCategory)
}

impl MainModel {
    pub fn new(acc_settings_folder: Box<Path>) -> Self {
        Self {
            acc_settings_folder,
            view_list: Vec::new(),
            unflitered_list: Vec::new(),
            text_filter: "".to_string(),
            sort: Sort::Asc(SortCategory::Name)
        }
    }

    pub fn update(&mut self) -> &Vec<Setup> {
        self.unflitered_list = crawler::find_setups(&self.acc_settings_folder);

        //Filter
        self.view_list = if self.text_filter.is_empty() {
            clone_list(&self.unflitered_list)
        } else {
            filtered_clone_list(&self.unflitered_list, self.text_filter.as_str())
        };

        //Sort
        sort_list(&mut self.view_list, &self.sort);


        &self.view_list
    }

    pub fn clear_filter(&mut self) {
        self.text_filter = "".to_string();
        self.sort = Sort::Asc(SortCategory::Name);
    }
}

fn clone_list(list: &Vec<Setup>) -> Vec<Setup> {
    let mut copy: Vec<Setup> = Vec::with_capacity(list.len());

    for item in list {
        copy.push(item.clone());
    }

    copy
}

fn filtered_clone_list(list: &Vec<Setup>, text: &str) -> Vec<Setup> {
    let mut copy: Vec<Setup> = Vec::with_capacity(list.len());

    let term = text.to_lowercase();

    for item in list {
        if item.name.to_lowercase().contains(&term) ||
            renamer::to_clean_car_name(&item.car_name).to_lowercase().contains(&term) ||
            renamer::to_clean_track_name(&item.track).to_lowercase().contains(&term) ||
            item.date.to_lowercase().contains(&term) {
            
                copy.push(item.clone());
        }
    }

    copy
}

fn sort_list(list: &mut Vec<Setup>, criteria: &Sort) {
    let mut index = 1;

    while index < list.len() {
        if !ordered(&list[index - 1], &list[index], &criteria)
        {
            let mut cursor:usize = 0;
            while cursor < (index - 1) && ordered(&list[cursor],&list[index], &criteria){
                cursor = cursor + 1;
            }

            let val = list[index].clone();
            list.remove(index);
            list.insert(cursor, val);
        }

        index = index + 1;
    }
}

fn ordered(s0: &Setup, s1: &Setup, criteria: &Sort) -> bool {
    
    let cat = match criteria { Sort::Asc(cat) => cat, Sort::Desc(cat) => cat };
    
    let (v0, v1) = match cat {
        SortCategory::Name => (&s0.name, &s1.name),
        SortCategory::Track => (&s0.track, &s1.track),
        SortCategory::Car => (&s0.car_name, &s1.car_name),
        SortCategory::Date => (&s0.date, &s1.date)
    };

    match v0.to_lowercase().cmp(&v1.to_lowercase()) {
        Ordering::Greater => match criteria { Sort::Asc(_) => false, Sort::Desc(_) => true },
        Ordering::Less => match criteria { Sort::Asc(_) => true, Sort::Desc(_) => false },
        Ordering::Equal => true
    }
}
