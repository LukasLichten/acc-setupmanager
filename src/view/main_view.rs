use fltk::{window::Window, group::{VGrid, Group}, button::Button, prelude::*, enums::{Color, Align, FrameType, Font}, image::PngImage};
use fltk_table::{SmartTable, TableOpts };

use crate::{model::main_model::MainModel, backend::renamer};

pub const WINDOW_WIDTH: i32 = 800;
pub const WINDOW_HEIGHT: i32 = 550;

#[derive(Debug, Clone)]
pub struct Controlls {
    pub window: Window,
    pub setup_table: SmartTable,
    pub setup_table_opts: TableOpts,
    setup_table_addopts: AdditionalTableOpts,
    setup_table_mount: Group,
    btn_group_mount: Group,
    btn_vgrid: VGrid,
    pub btn_filter: Button,
    pub btn_add: Button,
    pub btn_move: Button,
    pub btn_rename: Button,
    pub btn_del: Button,
    pub btn_refresh: Button,
    pub btn_export: Button,
    pub model: MainModel
}

impl Controlls {
    pub fn set_row_count(&mut self, number: i32) {
        if number == self.setup_table_opts.rows {
            // return; //No change required

            // Actually, we need to set the same numbers, as it triggers the redraw we want
            // Else Sort operations will not redraw all rows till the user scrolls
            // So do not readd this shortcut
        }

        self.setup_table_opts.rows = number;

        self.setup_table.set_opts(self.setup_table_opts.clone());
        self.readd_additional_opts();
    }

    fn  readd_additional_opts(&mut self) {
        self.setup_table.set_additonal_table_opts(&self.setup_table_addopts);

        self.setup_table.set_col_header_value(0, "Name");
        self.setup_table.set_col_header_value(1, "Track");
        self.setup_table.set_col_header_value(2, "Car");
        self.setup_table.set_col_header_value(3, "Date");
    }

    pub fn update_model(&mut self) {
        self.model.update();
        self.setup_table.unset_selection();
        self.re_render_table();
    }

    pub fn re_render_table(&mut self) {
        self.set_row_count(self.model.view_list.len() as i32);

        let mut i = 0;
        for set in &self.model.view_list {
            self.setup_table.set_cell_value(i, 0, set.name.as_str());
            self.setup_table.set_cell_value(i, 1, renamer::to_clean_track_name(set.track.as_str()));
            self.setup_table.set_cell_value(i, 2, renamer::to_clean_car_name(set.car_name.as_str()));
            self.setup_table.set_cell_value(i, 3, set.date.as_str());

            i += 1;
        }
    }

    /// To compute where a subwindow needs 
    pub fn get_sub_window_pos(&mut self, width: i32, height: i32) -> (i32, i32) {
        (self.window.x() + self.window.width() / 2 - width / 2, self.window.y() + self.window.height() / 2 - height / 2)
    }

    /// Resizes the window
    pub fn resize(&mut self, width: i32, height: i32) {
        let table_w = width - 200;
        
        let name_w = (table_w / 3).max(200);
        let track_w = (table_w / 46 * 10).max(130);
        let car_w = (table_w / 12 + track_w).max(180);
        let date_w = (table_w - 18 - (name_w + track_w + car_w)).max(72);

        self.setup_table_mount.resize(self.setup_table_mount.x(), self.setup_table_mount.y(), table_w, height - 5);

        self.setup_table.set_col_width(0,name_w); 
        self.setup_table.set_col_width(1,track_w);
        self.setup_table.set_col_width(2,car_w); 
        self.setup_table.set_col_width(3,date_w); 

        self.btn_group_mount.resize(self.setup_table_mount.x() + table_w + 25, self.setup_table_mount.y(), 150, height-5);
        self.btn_vgrid.resize(self.btn_group_mount.x(), (self.btn_group_mount.y() + (height - 300 - 5) / 5 * 2).max(3), 150, 300);
        

    }
}

#[derive(Debug, Clone)]
struct AdditionalTableOpts {
    col_show_headers: bool,
    col_allow_resize: bool,
    row_show_headers: bool,
    row_allow_resize: bool
    
}

impl Default for AdditionalTableOpts{
    fn default() -> Self {
        Self {
            col_show_headers: true,
            col_allow_resize: true,
            row_show_headers: true,
            row_allow_resize: true,
        }
    }
}

pub fn create_window(model: MainModel) -> Controlls {
    let mut wind = Window::default()
        .with_size(WINDOW_WIDTH, WINDOW_HEIGHT)
        .with_label("ACC Setup Manager")
        .center_screen();
    
    wind.set_color(Color::White);
    wind.set_icon(get_icon());
    
    //Because Smart Table does not implemented WidgetExt properly, so it is not possible to arrange the buttons next to it directly
    //But we can put the Table into a group, and then arrange the buttons next to it
    let table_mount = Group::default() 
        .with_size(600, 545)
        .with_align(Align::Left)
        ;

    let table_opts = TableOpts {
        cols:4,
        editable: false,
        header_frame: FrameType::FlatBox,
        cell_font: Font::Helvetica,
        cell_font_size: 11,
        cell_border_color: Color::Light2,
        header_color: Color::Black,
        header_font_color: Color::White,
        header_font_size: 14,
        header_font: Font::HelveticaBold,

        ..Default::default()
    };
    let mut tab = SmartTable::default_fill()
        .with_opts(table_opts.clone())
        ;
    
    tab.set_color(Color::White);

    let table_addopts = AdditionalTableOpts {
        col_show_headers: true,
        col_allow_resize: false,
        row_show_headers: false,
        row_allow_resize: false,

        ..Default::default()
    };
    
    tab.set_additonal_table_opts(&table_addopts);
    tab.set_col_header_value(0, "Name");
    tab.set_col_header_value(1, "Track");
    tab.set_col_header_value(2, "Car");
    tab.set_col_header_value(3, "Date");
    
    tab.set_col_width(0,200); //Name
    tab.set_col_width(1,130); //Track
    tab.set_col_width(2,180); //Car
    tab.set_col_width(3,72); //Date
    tab.set_col_header_height(26);

    tab.end();
    table_mount.end();

    //This exists to be able to center the buttons vertically
    let button_group_mount = Group::default()
        .with_size(150, 545)
        .right_of(&table_mount, 25)
        ;
    
    let mut vgrid = VGrid::default()
        .with_size(150, 300)
        .center_of(&button_group_mount)
        ;
    vgrid.set_params(7, 1, 15);
    
    let btn_filter = Button::default().with_label("Sort/Filter").with_color(Color::White, Color::Black);
    let btn_add = Button::default().with_label("Add").with_color(Color::White, Color::Black);
    let btn_move = Button::default().with_label("Move/Copy").with_color(Color::White, Color::Black);
    let btn_rename = Button::default().with_label("Rename").with_color(Color::White, Color::Black);
    let btn_del = Button::default().with_label("Delete").with_color(Color::White, Color::Black);
    let btn_refresh = Button::default().with_label("Refresh").with_color(Color::White, Color::Black);
    let btn_export = Button::default().with_label("Export").with_color(Color::White, Color::Black);

    vgrid.end();
    button_group_mount.end();

    wind.end();
    wind.make_resizable(true);
    wind.show();

    Controlls {
        window: wind,
        setup_table: tab,
        setup_table_opts: table_opts,
        setup_table_addopts: table_addopts,
        setup_table_mount: table_mount,
        btn_group_mount: button_group_mount,
        btn_vgrid: vgrid,
        btn_filter,
        btn_add,
        btn_move,
        btn_rename,
        btn_del,
        btn_refresh,
        btn_export,
        model
    }
}

pub fn get_icon() -> Option<PngImage> {
    let logo_in_bytes = include_bytes!("../assets/logo128.png");

    let logo_loaded = PngImage::from_data(logo_in_bytes);
    
    logo_loaded.ok()
}

//Helper Traits
trait AddOpts {
    fn set_additonal_table_opts(&mut self, opts: &AdditionalTableOpts);
}

impl AddOpts for SmartTable {
    fn set_additonal_table_opts(&mut self, opts: &AdditionalTableOpts){
        self.set_col_header(opts.col_show_headers);
        self.set_col_resize(opts.col_allow_resize);
        self.set_row_header(opts.row_show_headers);
        self.set_row_resize(opts.row_allow_resize);
    }
}

pub trait Colorize {
    fn with_color(self, label_color: Color, back_color: Color) -> Self;
}

impl Colorize for Button {
    fn with_color(mut self, label_color: Color, back_color: Color) -> Self {
        self.set_color(back_color);
        self.set_label_color(label_color);

        self
    }
}
