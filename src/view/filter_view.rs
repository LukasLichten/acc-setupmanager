use fltk::{window::Window, enums::{Color}, menu::Choice, prelude::{WidgetExt, MenuExt, GroupExt, InputExt, WindowExt}, input::{self, Input}, button::Button};

use crate::model::main_model::{Sort,SortCategory};

use super::main_view::Colorize;

const WIDTH :i32 = 450;
const HEIGHT :i32 = 190;

pub struct FilterControlls {
    pub window: Window,
    pub filter_input: Input,
    pub btn_set_filter: Button,
    pub category_choice: Choice,
    pub direction_choice: Choice,
    pub btn_reset: Button
}

impl FilterControlls {
    pub fn close_window(&mut self) {
        self.window.hide();
    }
}

pub fn create_window(controlls: &mut super::main_view::Controlls) -> FilterControlls {
    controlls.window.end();
    controlls.window.show(); //This has to be called to make this a seperate window

    let (win_pos_x, win_pos_y) = controlls.get_sub_window_pos(WIDTH, HEIGHT);

    let mut win = Window::default()
        .with_size(WIDTH, HEIGHT)
        .with_pos(win_pos_x, win_pos_y)
        .with_label("Sort/Filter")
        ;
    
    win.set_color(Color::White);
    win.set_icon(super::main_view::get_icon());

    let top_padding = 20;
    let item_height = 42;
    let padding = 12;

    let mut text_input = input::Input::default()
        .with_pos(5, top_padding)
        .with_size(WIDTH - 55, item_height);
    text_input.set_value(&controlls.model.text_filter);
    
    let btn_set_filter = Button::default()
        .with_color(Color::White, Color::Black)
        .with_label("🔍")
        .with_pos(WIDTH - 45, top_padding)
        .with_size(40, item_height);

    let mut category_choice = Choice::default()
        .with_pos(5, top_padding + padding + item_height)
        .with_size(WIDTH - 90, item_height);
    category_choice.add_choice("Name");
    category_choice.add_choice("Track");
    category_choice.add_choice("Car");
    category_choice.add_choice("Date");

    let mut direction_choice = Choice::default()
        .with_pos(WIDTH - 80, top_padding + padding + item_height)
        .with_size(75, item_height);
    direction_choice.add_choice("Asc");
    direction_choice.add_choice("Desc");

    let cat = match &controlls.model.sort {
        Sort::Asc(cat) => {
            direction_choice.set_value(0);
            cat
        },
        Sort::Desc(cat) => {
            direction_choice.set_value(1);
            cat
        }
    };

    category_choice.set_value(match cat {
        SortCategory::Name => 0,
        SortCategory::Track => 1,
        SortCategory::Car => 2,
        SortCategory::Date => 3
    });

    let btn_reset = Button::default()
        .with_color(Color::White, Color::Black)
        .with_label("Reset")
        .with_pos(5, top_padding + 2* padding + 2* item_height)
        .with_size(WIDTH - 10, item_height);
    
    
    win.end();
    win.show();

    
    
    FilterControlls {
        window: win,
        category_choice,
        direction_choice,
        filter_input: text_input,
        btn_set_filter,
        btn_reset
    }
}