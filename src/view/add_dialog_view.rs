use fltk::{window::Window, enums::Color, menu::Choice, prelude::{WidgetExt, MenuExt, GroupExt, InputExt, WindowExt}, group::VGrid, input::{self, Input}, button::{Button, CheckButton}};

use crate::{backend::{renamer, json_worker, crawler}, model::add_dialog_model::{AddSetupModel, TransferMode}};

use super::main_view::Colorize;

const WIDTH :i32 = 350;
const HEIGHT :i32 = 350;

pub struct AddControlls {
    pub window: Window,
    pub btn_save: Button,
    pub name_input: Input,
    pub car_choice: Choice,
    pub track_choice: Choice,
    pub btn_copy: CheckButton,
    pub model: AddSetupModel
}

impl AddControlls {
    pub fn close_window(&mut self) {
        self.window.hide();
    }
}

pub fn create_window(controlls: &mut super::main_view::Controlls, model: AddSetupModel) -> AddControlls {
    controlls.window.end();
    controlls.window.show(); //This has to be called to make this a seperate window

    let mut win_pos = controlls.get_sub_window_pos(WIDTH, HEIGHT);

    if model.drag_drop {
        win_pos.0 = win_pos.0 + super::main_view::WINDOW_WIDTH
    }

    let mut win = Window::default()
        .with_size(WIDTH, HEIGHT)
        .with_pos(win_pos.0, win_pos.1)
        .with_label("Add Setup")
        ;
    
    win.set_color(Color::White);
    win.set_icon(super::main_view::get_icon());
    
    
    let mut group = VGrid::default()
        .with_size(WIDTH - 10, HEIGHT - 50)
        .with_pos(5, 25);

    group.set_params(5, 1, 20);

    let mut text_input = input::Input::default();
    
    
    text_input.set_value(crawler::get_file_name(&model.path).as_str());
    
    let mut car_choice = Choice::default();

    let list_of_cars = renamer::list_of_cars();

    for car in &list_of_cars {
        car_choice.add_choice(renamer::to_clean_car_name(car.as_str()));
    }
    
    //Pre selectes the car already, if possible
    if let Ok(data) = json_worker::read_json(&model.path) {
        if let Some(curr) = data["carName"].as_str() {
            if let Ok(index) = list_of_cars.binary_search(&curr.to_string()) {
                car_choice.set_value(index as i32);
            }
        }
    }

    let mut track_choice = Choice::default();
    
    let list_of_tracks = renamer::list_of_tracks();

    for track in &list_of_tracks {
        track_choice.add_choice(renamer::to_clean_track_name(track.as_str()));
    }

    //No preselect for tracks so far

    let mut btn_copy = CheckButton::default()
        .with_label("Copy");

    btn_copy.set_label_size(18);
    
    let mut btn_save = Button::default()
        .with_color(Color::White, Color::Black);
    
    match model.mode {
        TransferMode::Add => {btn_copy.hide(); btn_save.set_label("Add");},
        TransferMode::Copy => {btn_copy.set_checked(true); btn_save.set_label("Copy");},
        TransferMode::Move => {btn_copy.set_checked(false); btn_save.set_label("Move");}
    }
    
    group.end();
    win.end();
    win.show();

    
    
    AddControlls {
        window: win,
        btn_save,
        car_choice,
        track_choice,
        name_input: text_input,
        btn_copy,
        model
    }
}