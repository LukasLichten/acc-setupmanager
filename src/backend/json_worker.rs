use std::{path::Path, fs};

use json::JsonValue;


pub fn read_json(file: &Path) -> json::Result<JsonValue>{
    if let Ok(read) = fs::read_to_string(file) {
        return json::parse(read.as_str());
    }

    Err(json::Error::WrongType("File System Error".to_string()))
}