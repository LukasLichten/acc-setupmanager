use std::{fs, path::{PathBuf, Path}, time::SystemTime, str::FromStr};
use futures::{self, executor::block_on, future::join_all};
use chrono::{Duration, Local, NaiveDate, TimeZone, Utc};
use proton_finder::GameDrive;
use crate::model::main_model::Setup;

//Folder Strcuture in ACC:
//User/Documents
//  ACC
//      Setups
//          [cars]
//              [tracks]
//                  [setup.json]
pub const ACC_ROOT_FOLDER_NAME: &str = "Assetto Corsa Competizione";
pub const ACC_SETUP_FOLDER_NAME: &str = "Setups";

pub const DATE_FORMAT_STR: &str = "%Y.%m.%d";

pub const FILE_ENDING: &str = "json";

pub fn get_acc_folder() -> Result<Option<PathBuf>,Option<PathBuf>> {
    let (game_drive, err) = match proton_finder::get_game_drive(805550) {
        Ok(res) => (res, false),
        Err(res) => (res, true)
    };


    fn internal_pathing(game_drive: Option<GameDrive>) -> Option<PathBuf> {
        let mut root_path = game_drive?.document_dir()?;
        root_path.push(ACC_ROOT_FOLDER_NAME);

        if root_path.is_dir() {
            let mut builder = root_path.clone();

            builder.push(ACC_SETUP_FOLDER_NAME);

            if !builder.exists() {
                let _ = std::fs::create_dir(builder.as_path());
            }
            if builder.is_dir() {
                return Some(root_path);
            }
        }

        None
    }
    
    let res = internal_pathing(game_drive);
    match err {
        false => Ok(res),
        true => Err(res)
    }
}

pub fn find_setups(root_path: &Path) -> Vec<Setup> {
    block_on(find_setups_async(root_path))
}

async fn find_setups_async(root_path: &Path) -> Vec<Setup> {
    let mut setup_folder = root_path.to_path_buf();
    setup_folder.push(ACC_SETUP_FOLDER_NAME);


    let top_dir_res = fs::read_dir(setup_folder);

    if top_dir_res.is_err() {
        //Do error handle
        let list: Vec<Setup> = Vec::new();
        return list;
    }
    let car_dir = top_dir_res.unwrap();
    
    let mut futures  = vec![scan_car_folder(PathBuf::new())]; //car_dir.count();


    for dir_res in car_dir {
        if dir_res.is_ok() {
            let entry = dir_res.unwrap();
            if entry.metadata().unwrap().is_dir() {
                let fut = scan_car_folder(entry.path());
                futures.push(fut);
            }
        }
        //We just ignore those with error
    }

    let output= join_all(futures).await;

    

    let mut list : Vec<Setup> = Vec::new();
    for mut out in output {
        list.append(&mut out);
    }

    list
}

async fn scan_car_folder(path: PathBuf) -> Vec<Setup> {
    if !path.exists() {
        let list: Vec<Setup> = Vec::new();
        return list;
    }


    let car_name = get_file_name(path.as_path());
    
    let super_dir_res = fs::read_dir(path);

    if super_dir_res.is_err() {
        //Do error handle
    }
    let super_dir = super_dir_res.unwrap();
    

    let mut list : Vec<Setup> = Vec::new();

    for dir_res in super_dir {
        if dir_res.is_ok() {
            let entry = dir_res.unwrap();
            if entry.metadata().unwrap().is_dir() {
                let mut track_list = scan_track_folder(entry.path(), car_name.clone());
                list.append(&mut track_list);
            }
        }
    }

    list
}

fn scan_track_folder(path: PathBuf, car_name: String) -> Vec<Setup> {
    let track_name = get_file_name(path.as_path());

    let track_dir_res = fs::read_dir(path);

    if track_dir_res.is_err() {
        //Do errror handle
    }
    let track_dir = track_dir_res.unwrap();

    let mut list : Vec<Setup> = Vec::new();

    for setup_res in track_dir {
        if let Ok(entry) = setup_res {

            if entry.metadata().unwrap().is_file() {
                list.push(read_setup_file(entry.path(), track_name.clone(), car_name.clone()));
            }
        }
    }

    list
}

fn read_setup_file(path: PathBuf, track: String, car: String) -> Setup {
    let mod_date = path.metadata().unwrap().modified().unwrap();
    
    let file_name = get_file_name(path.as_path());

    Setup {
        path: path.into_boxed_path(),
        car_name: car,
        track,
        name: file_name,
        date: format_date(get_time(mod_date))
    }
}

pub fn get_file_name(path: &Path) -> String { //TODO: Do actual error correcting
    String::from_str(path.file_stem().unwrap().to_str().unwrap()).unwrap()
}

//Because rust does not have a way to format time per std, and even with chrono and other crates the std::systemtime can not be converted easily
fn get_time(sys_time: SystemTime) -> NaiveDate {
    let t = sys_time.duration_since(SystemTime::UNIX_EPOCH);
    let epoch = Utc.timestamp_opt(0, 0).unwrap();

    if let Ok(dur) = t {
        let chrono_res = Duration::from_std(dur);

        if let Ok(chrono_dur) = chrono_res {
            let out = epoch + chrono_dur;
            return Local.from_utc_datetime(&out.naive_utc()).date_naive();
        }
    }

    Local.from_utc_datetime(&epoch.naive_utc()).date_naive()
}

fn format_date(date: NaiveDate) -> String {
    date.format(DATE_FORMAT_STR).to_string()
}

pub fn check_file_type(path: &PathBuf) -> bool{
    if let Some(os_str) = path.extension() {
        if let Some(val) = os_str.to_str() {
            return FILE_ENDING == val;
        }
    }
    
    false
}

//Builds a Path, also checks if the file already exists (still returns path in Err), and creates the folders if necessary
pub fn check_for_file(root_dir: &Path, car_name: &str, track_name: &str, file_name: &str) -> Result<PathBuf, PathBuf> {
    let mut builder = root_dir.to_path_buf();
    builder.push(ACC_SETUP_FOLDER_NAME);

    if builder.exists() {
        builder.push(car_name);
        
        if !builder.exists() {
            //creating car folder
            fs::create_dir(builder.as_path()).unwrap();
        }

        builder.push(track_name);

        if !builder.exists() {
            //creating track folder
            fs::create_dir(builder.as_path()).unwrap();
        }
    } else {
        panic!("ACC Setup folder not found at {} Attempt to provision folders for setup failed", builder.to_str().unwrap());
    }

    

    builder.push(file_name);

    let extension = if let Some(curr_ext) = builder.extension() {
        if !curr_ext.eq_ignore_ascii_case(FILE_ENDING) {
            format!("{}.{}", curr_ext.to_str().unwrap(), FILE_ENDING)
        } else {
            FILE_ENDING.to_owned()
        }
    } else { FILE_ENDING.to_owned() };
    builder.set_extension(extension);

    if builder.exists() {
        //file exists, we still return the path, but in the Error
        return Result::Err(builder);
    }

    Result::Ok(builder)
}
