pub fn to_clean_car_name(folder_name: &str) -> &str {
    match folder_name {
        "alpine_a110_gt4" => "Alpine A110 GT4",
        "amr_v8_vantage_gt3" => "AMR V8 Vantage GT3",
        "amr_v8_vantage_gt4" => "AMR Vantage GT4",
        "amr_v12_vantage_gt3" => "AMR V12 Vantage GT3",
        "audi_r8_gt4" => "Audi R8 GT4",
        "audi_r8_lms" => "Audi R8 LMS GT3",
        "audi_r8_lms_evo" => "Audi R8 LMS GT3 Evo",
        "audi_r8_lms_evo_ii" => "Audi R8 LMS GT3 Evo II",
        "audi_r8_lms_gt2" => "Audi R8 LMS GT2",
        "bentley_continental_gt3_2016" => "Bentley Continental GT3 2015",
        "bentley_continental_gt3_2018" => "Bentley Continental GT3 2018",
        "bmw_m2_cs_racing" => "BMW M2 CS TCX",
        "bmw_m4_gt3" => "BMW M4 GT3",
        "bmw_m4_gt4" => "BMW M4 GT4",
        "bmw_m6_gt3" => "BMW M6 GT3",
        "chevrolet_camaro_gt4r" => "Chevrolet Camaro GT4",
        "ferrari_296_gt3" => "Ferrari 296 GT3",
        "ferrari_488_challenge_evo" => "Ferrari Challenge Evo GTC",
        "ferrari_488_gt3" => "Ferrari 488 GT3",
        "ferrari_488_gt3_evo" => "Ferrari 488 GT3 Evo",
        "ford_mustang_gt3" => "Ford Mustang GT3",
        "ginetta_g55_gt4" => "Ginetta G55 GT4",
        "honda_nsx_gt3" => "Honda NSX GT3",
        "honda_nsx_gt3_evo" => "Honda NSX GT3 Evo",
        "jaguar_g3" => "Jaguar G3 GT3",
        "ktm_xbow_gt2" => "KTM Xbow GT2",
        "ktm_xbow_gt4" => "KTM Xbow GT4",
        "lamborghini_gallardo_rex" => "Lamborghini Gallardo Rex GT3",
        "lamborghini_huracan_gt3" => "Lamborghini Huaracan GT3",
        "lamborghini_huracan_gt3_evo" => "Lamborghini Huaracan GT3 Evo",
        "lamborghini_huracan_gt3_evo2" => "Lamborghini Huaracan GT3 Evo 2",
        "lamborghini_huracan_st" => "Lamborghini Huaracan ST GTC",
        "lamborghini_huracan_st_evo2" => "Lamborghini Huaracan ST Evo GTC",
        "lexus_rc_f_gt3" => "Lexus Rc-F GT3",
        "maserati_mc20_gt2" => "Maserati MC20 GT2",
        "maserati_mc_gt4" => "Maserati MC GT4",
        "mclaren_570s_gt4" => "McLaren 570S GT4",
        "mclaren_650s_gt3" => "McLaren 650S GT3",
        "mclaren_720s_gt3" => "McLaren 720S GT3",
        "mclaren_720s_gt3_evo" => "McLaren 720S GT3 Evo",
        "mercedes_amg_gt2" => "Mercedes AMG GT2",
        "mercedes_amg_gt3" => "Mercedes AMG GT3",
        "mercedes_amg_gt3_evo" => "Mercedes AMG GT3 Evo",
        "mercedes_amg_gt4" => "Mercedes AMG GT4",
        "nissan_gt_r_gt3_2017" => "Nissan GT-R GT3 2015",
        "nissan_gt_r_gt3_2018" => "Nissan GT-R GT3 2018",
        "porsche_718_cayman_gt4_mr" => "Porsche 718 Cayman GT4",
        "porsche_935" => "Porsche 935 GT2",
        "porsche_991_gt2_rs_mr" => "Porsche 991-II GT2 RS",
        "porsche_991ii_gt3_cup" => "Porsche 991.2 GT3Cup GTC",
        "porsche_991ii_gt3_r" => "Porsche 991-II GT3R",
        "porsche_991_gt3_r" => "Porsche 991 GT3R",
        "porsche_992_gt3_cup" => "Porsche 992 GT3Cup GTC",
        "porsche_992_gt3_r" => "Porsche 992 GT3R",
        _ => folder_name,
    }
}

pub fn list_of_cars() -> Vec<String> {
    let mut list: Vec<String> = Vec::with_capacity(54);
    list.push("alpine_a110_gt4".to_string());
    list.push("amr_v12_vantage_gt3".to_string());
    list.push("amr_v8_vantage_gt3".to_string());
    list.push("amr_v8_vantage_gt4".to_string());
    list.push("audi_r8_gt4".to_string());
    list.push("audi_r8_lms".to_string());
    list.push("audi_r8_lms_evo".to_string());
    list.push("audi_r8_lms_evo_ii".to_string());
    list.push("audi_r8_lms_gt2".to_string());
    list.push("bentley_continental_gt3_2016".to_string());
    list.push("bentley_continental_gt3_2018".to_string());
    list.push("bmw_m2_cs_racing".to_string());
    list.push("bmw_m4_gt3".to_string());
    list.push("bmw_m4_gt4".to_string());
    list.push("bmw_m6_gt3".to_string());
    list.push("chevrolet_camaro_gt4r".to_string());
    list.push("ferrari_296_gt3".to_string());
    list.push("ferrari_488_challenge_evo".to_string());
    list.push("ferrari_488_gt3".to_string());
    list.push("ferrari_488_gt3_evo".to_string());
    list.push("ford_mustang_gt3".to_string());
    list.push("ginetta_g55_gt4".to_string());
    list.push("honda_nsx_gt3".to_string());
    list.push("honda_nsx_gt3_evo".to_string());
    list.push("jaguar_g3".to_string());
    list.push("ktm_xbow_gt2".to_string());
    list.push("ktm_xbow_gt4".to_string());
    list.push("lamborghini_gallardo_rex".to_string());
    list.push("lamborghini_huracan_gt3".to_string());
    list.push("lamborghini_huracan_gt3_evo".to_string());
    list.push("lamborghini_huracan_gt3_evo2".to_string());
    list.push("lamborghini_huracan_st".to_string());
    list.push("lamborghini_huracan_st_evo2".to_string());
    list.push("lexus_rc_f_gt3".to_string());
    list.push("maserati_mc20_gt2".to_string());
    list.push("maserati_mc_gt4".to_string());
    list.push("mclaren_570s_gt4".to_string());
    list.push("mclaren_650s_gt3".to_string());
    list.push("mclaren_720s_gt3".to_string());
    list.push("mclaren_720s_gt3_evo".to_string());
    list.push("mercedes_amg_gt2".to_string());
    list.push("mercedes_amg_gt3".to_string());
    list.push("mercedes_amg_gt3_evo".to_string());
    list.push("mercedes_amg_gt4".to_string());
    list.push("nissan_gt_r_gt3_2017".to_string());
    list.push("nissan_gt_r_gt3_2018".to_string());
    list.push("porsche_718_cayman_gt4_mr".to_string());
    list.push("porsche_935".to_string());
    list.push("porsche_991_gt2_rs_mr".to_string());
    list.push("porsche_991_gt3_r".to_string());
    list.push("porsche_991ii_gt3_cup".to_string());
    list.push("porsche_991ii_gt3_r".to_string());
    list.push("porsche_992_gt3_cup".to_string());
    list.push("porsche_992_gt3_r".to_string());

    list
}

pub fn to_clean_track_name(folder_name: &str) -> &str{
    match folder_name {
        "Barcelona" => "Barcelona",
        "brands_hatch" => "Brands Hatch",
        "cota" => "Circuit Of The Americas",
        "donington" => "Donington",
        "Hungaroring" => "Hungaroring",
        "Imola" => "Imola",
        "indianapolis" => "Indianapolis",
        "Kyalami" => "Kyalami",
        "Laguna_Seca" => "Laguna Seca",
        "misano" => "Misano",
        "monza" => "Monza",
        "mount_panorama" => "Mount Panorama",
        "nurburgring" => "Nürburgring",
        "nurburgring_24h" => "Nordschleife",
        "oulton_park" => "Oulton Park",
        "Paul_Ricard" => "Paul Ricard",
        "red_bull_ring" => "Red Bull Ring",
        "Silverstone" => "Silverstone",
        "snetterton" => "Snetterton",
        "Spa" => "Spa Francorchamps",
        "Suzuka" => "Suzuka",
        "Valencia" => "Valencia",
        "watkins_glen" => "Watkins Glen",
        "Zandvoort" => "Zandvoort",
        "Zolder" => "Zolder",
        _ => folder_name,
    }
}

pub fn list_of_tracks() -> Vec<String> {
    let mut list: Vec<String> = Vec::with_capacity(25);
    list.push("Barcelona".to_string());
    list.push("brands_hatch".to_string());
    list.push("cota".to_string());
    list.push("donington".to_string());
    list.push("Hungaroring".to_string());
    list.push("Imola".to_string());
    list.push("indianapolis".to_string());
    list.push("Kyalami".to_string());
    list.push("Laguna_Seca".to_string());
    list.push("misano".to_string());
    list.push("monza".to_string());
    list.push("mount_panorama".to_string());
    list.push("nurburgring".to_string());
    list.push("nurburgring_24h".to_string());
    list.push("oulton_park".to_string());
    list.push("Paul_Ricard".to_string());
    list.push("Silverstone".to_string());
    list.push("snetterton".to_string());
    list.push("Spa".to_string());
    list.push("Suzuka".to_string());
    list.push("Valencia".to_string());
    list.push("watkins_glen".to_string());
    list.push("Zandvoort".to_string());
    list.push("Zolder".to_string());

    list
}
