use fltk::{app::{event_key, Sender}, enums::{Event, Key}, input::Input, prelude::{MenuExt, WidgetBase, WidgetExt}};

use crate::{view::{main_view::Controlls, filter_view}, model::main_model::{SortCategory, Sort}};

use super::main_controller::Message;

pub fn show_dialog(controlls: &mut Controlls, s: &Sender<Message>) -> Option<Input> {
    let mut dialog = filter_view::create_window(controlls);
    

    let s_clone = s.clone();
    //let mut c_clone = controlls.clone();

    dialog.category_choice.set_callback({
        let cat_choice = dialog.category_choice.clone();
        let dir_choice = dialog.direction_choice.clone();

        move |_| {
            s_clone.send(Message::SetSort(get_sort(cat_choice.value(), dir_choice.value())));
        }
    });

    dialog.direction_choice.set_callback({
        let cat_choice = dialog.category_choice.clone();
        let dir_choice = dialog.direction_choice.clone();

        move |_| {
            s_clone.send(Message::SetSort(get_sort(cat_choice.value(), dir_choice.value())));
        }
    });

    dialog.btn_set_filter.emit(s_clone, Message::SetTextFilter);

    // Handling enter key on textfield as confirm
    dialog.filter_input.handle(move |_,e| {
        match e {
            Event::KeyDown => {
                if event_key() == Key::Enter {
                    s_clone.send(Message::SetTextFilter);
                    
                    true
                } else {
                    false
                }
                
            },
            _ => false
        }
    });

    dialog.btn_reset.set_callback({
        move |_| {
            s_clone.send(Message::ResetFilter);
            dialog.window.hide()
        }
    });

    Option::Some(dialog.filter_input.clone())
}

fn get_sort(category: i32, dir: i32) -> Sort
{
    let cat: SortCategory = match category {
        0 => SortCategory::Name,
        1 => SortCategory::Track,
        2 => SortCategory::Car,
        3 => SortCategory::Date,
        _ => SortCategory::Name
    };

    match dir {
        0 => Sort::Asc(cat),
        1 => Sort::Desc(cat),
        _ => Sort::Asc(cat)
    }
}

