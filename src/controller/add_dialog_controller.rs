use std::{fs, path::PathBuf};

use fltk::{dialog::{FileDialog, FileDialogType, FileDialogOptions, self}, prelude::{WidgetExt, InputExt, MenuExt}, app::Sender};
use json::stringify_pretty;

use crate::{view::{main_view::Controlls, add_dialog_view::{self, AddControlls}}, backend::{crawler, renamer, json_worker}, model::add_dialog_model::{AddSetupModel, TransferMode}};

use super::main_controller::Message;

pub fn show_dialog(controlls: &mut Controlls, s: &Sender<Message>, mode: TransferMode) {
    let mut dialog = FileDialog::new(FileDialogType::BrowseMultiFile);
    dialog.set_filter("ACC Setup Json\t*.json");
    dialog.set_option(FileDialogOptions::UseFilterExt);
    dialog.show();

    let files = dialog.filenames();
    if files.len() == 0 {
        return; //Nothing selected, exiting
    }

    creat_dialog_for_files(&files, controlls, s, mode);
}

pub fn creat_dialog_for_files(list: &Vec<PathBuf>, controlls: &mut Controlls, s: &Sender<Message>, mode: TransferMode) {
    creat_dialog_for_files_drop(list, controlls, s, mode,false)
}

pub fn creat_dialog_for_files_drop(list: &Vec<PathBuf>, controlls: &mut Controlls, s: &Sender<Message>, mode: TransferMode, drag_drop: bool) {
    let mut files = list.clone();

    if let Some(item) = files.pop() {
        let model = AddSetupModel {
            path: item.clone().into_boxed_path(),
            next_files: files,
            mode,
            drag_drop
        };

        let dialog = add_dialog_view::create_window(controlls, model);
    
        run_dialog(dialog, controlls, s);
    }

    s.send(Message::Refresh);
}

fn run_dialog(mut dialog: AddControlls, controlls: &mut Controlls, s: &Sender<Message>) {

    let acc_root_store = (&controlls.model.acc_settings_folder).to_path_buf();
    let s_clone = s.clone();
    let mut c_clone = controlls.clone();

    dialog.btn_copy.set_callback({

        let btn_copy = dialog.btn_copy.clone();
        let mut btn_save = dialog.btn_save.clone();

        move |_| {
            if btn_copy.is_checked() {
                btn_save.set_label("Copy");
            } else {
                btn_save.set_label("Move");
            }
        }
    });

    dialog.btn_save.set_callback({
        let btn_copy = dialog.btn_copy.clone();
        move |_| {
            let name = dialog.name_input.value().trim().to_string();

            if dialog.track_choice.value() == -1 || dialog.car_choice.value() == -1 || name.as_str() == "" {
                //No car selected
                dialog::message_default("Please fill in the fields above.");
                return;
            }

            if match dialog.model.mode { TransferMode::Add => true, _ => false} {
                //Stay in add mode
            } else if btn_copy.is_checked() {
                dialog.model.mode = TransferMode::Copy;
            } else {
                dialog.model.mode = TransferMode::Move;
            }

            let list_of_cars = renamer::list_of_cars();

            if let Some(car_name) = &list_of_cars.get(dialog.car_choice.value() as usize) {
                if let Some(track_name) = &renamer::list_of_tracks().get(dialog.track_choice.value() as usize) {



                    //Check if the carmodel aligns with the model in the json
                    if let Ok(mut data) = json_worker::read_json(&dialog.model.path) {
                        if let Some(car_name_in_file) = data["carName"].as_str() {
                            if car_name_in_file != car_name.as_str() {
                                
                                //car name has changed
                                let val = dialog::choice2_default(format!("This setup was made for the {}, but you want to use it for the {}. Compatibility issues may arrise",
                                    renamer::to_clean_car_name(&car_name_in_file), renamer::to_clean_car_name(car_name.as_str())).as_str(), if (&dialog.model.next_files).is_empty() { "Cancel" } else { "Skip" }, "Edit", "Go Ahead");

                                match val {
                                    Some(2) => (), //proceed
                                    Some(1) => {
                                        if let Ok(index) = list_of_cars.binary_search(&car_name_in_file.to_string()) {
                                            dialog.car_choice.set_value(index as i32);
                                        }
                                        return;
                                    }, //edit
                                    Some(0) => {
                                        dialog.window.hide();
                                        creat_dialog_for_files(&dialog.model.next_files, &mut c_clone, &s_clone, dialog.model.mode);
                                        return;
                                    }, //exit
                                    None => return, //Dialog closed, not proceeding, leave settings as they are
                                    _ => return //edge case, same as above
                                }

                                //doing the edit
                                data["carName"] = car_name.as_str().into();
                                let mut temp = (&dialog.model.path).to_path_buf(); 
                                temp.set_extension("temp");

                                if let Err(err) = fs::write(temp.as_path(), stringify_pretty(data, 4)) {
                                    dialog::alert_default(format!("Failed to do necessary edit to {}, setup will not load ingame: {}", &name, err).as_str());
                                } else {
                                    dialog.model.path = temp.into_boxed_path();
                                }
                            }
                        }
                    }

                    //Copying operation
                    let res = crawler::check_for_file(acc_root_store.as_path(), car_name.as_str(), track_name.as_str(), name.as_str());

                    if res.is_err() {
                        //We need to ask the user before we overwrite the file
                        let val = dialog::choice2_default(format!("Setup {} already exists for car {} at {}. Do you want to Overwrite?",&name, renamer::to_clean_car_name(car_name.as_str()), renamer::to_clean_track_name(track_name.as_str())).as_str(), "Skip", "Edit", "Overwrite");

                        match val {
                            Some(2) => (), //proceed
                            Some(1) => {clean_up(&mut dialog.model); return;}, //edit
                            Some(0) => {dialog.window.hide(); clean_up(&mut dialog.model); creat_dialog_for_files(&dialog.model.next_files, &mut c_clone, &s_clone, dialog.model.mode); return;}, //exit
                            None => {clean_up(&mut dialog.model); return;}, //dialog closed, doing same as edit
                            _ => {clean_up(&mut dialog.model); return;} //edge case, doing same es edit
                        }
                    }

                    let dest = match res {
                        Ok(k) => k,
                        Err(e) => e
                    };

                    if let Err(err) = fs::copy(&dialog.model.path, dest) {
                        dialog::alert_default(format!("Failed to write setup: {}", err).as_str());
                    }

                    dialog.window.hide();
                    clean_up(&mut dialog.model);
                    

                    if let Err(err) = match dialog.model.mode  {
                        TransferMode::Move => fs::remove_file(&dialog.model.path), //Deleting remnant
                        TransferMode::Copy => Ok(()),
                        TransferMode::Add => Ok(())
                    } {
                        dialog::alert_default(format!("Failure to move setup. It was copyed, but original could not be deleted: {}", err).as_str());
                    }

                    creat_dialog_for_files(&dialog.model.next_files, &mut c_clone, &s_clone, dialog.model.mode);
                }
            }

            
        }
    });
}

fn clean_up(model: &mut AddSetupModel) {
    let mut path = (&model.path).to_path_buf();

    if let Some(os_str) = path.extension() {
        if let Some(val) = os_str.to_str() {
            if "temp" == val {
                //Temp file
                
                if let Err(err) = fs::remove_file(path.as_path()) {
                    panic!("This should not happen... {}", err);
                }
                path.set_extension(crawler::FILE_ENDING);
                model.path = path.into_boxed_path();
            }
        }
    }
}