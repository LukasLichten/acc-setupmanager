use std::{fs, path::PathBuf, str::FromStr};

use fltk::{app::{self, Sender}, prelude::{WidgetExt, TableExt, WidgetBase, InputExt}, dialog::{self, FileDialog, FileDialogType, FileDialogOptions}, enums::Event};

use crate::{view::main_view::Controlls, model::{main_model::{Setup, Sort}, add_dialog_model::TransferMode}, backend::crawler};

use super::{add_dialog_controller, filter_controller};

#[derive(Debug, Clone, Copy)]
pub enum Message {
    Filter,
    Add,
    Move,
    Rename,
    Delete,
    Export,
    Refresh,
    CloseWindow,
    Resize(i32, i32),
    //Signalling
    SetTextFilter,
    SetSort(Sort),
    ResetFilter
}

pub fn run_app(app: app::App, mut controlls: Controlls) {
    let (s, r) = app::channel::<Message>();

    controlls.update_model();

    
    controlls.btn_filter.emit(s, Message::Filter);
    controlls.btn_add.emit(s, Message::Add);
    controlls.btn_move.emit(s, Message::Move);
    controlls.btn_rename.emit(s, Message::Rename);
    controlls.btn_del.emit(s, Message::Delete);
    controlls.btn_refresh.emit(s, Message::Refresh);
    controlls.btn_export.emit(s, Message::Export);
    controlls.window.emit(s, Message::CloseWindow);


    controlls.window.resize_callback(move |_, _pos_x, _pos_y, size_x, size_y| {
        s.send(Message::Resize(size_x, size_y))
    });

    let mut con_clone = (&controlls).clone();
    controlls.setup_table.handle({
        let mut dnd = false;
        let mut released = false;
        
        move |_, ev| match ev {
            Event::DndEnter => {
                dnd = true;
                true
            }
            Event::DndDrag => true,
            Event::DndRelease => {
                released = true;
                true
            }
            Event::Paste => {
                if dnd && released {
                    let content = app::event_text();
                    let split: Vec<&str> = content.split('\n').collect();
                    
                    let mut files: Vec<PathBuf> = Vec::with_capacity(split.len()); 
                    for item in split {
                        let item = if let Some(stripped) = item.strip_prefix("file://") {
                            // file prefix needs stripping on Linux
                            stripped
                        } else {
                            item
                        };

                        let item = if let Ok(res) = urlencoding::decode(item) {
                            // paths (at least under Linux) are URLEncoded, so we convert them
                            res.into_owned()
                        } else {
                            item.to_owned()
                        };

                        #[allow(irrefutable_let_patterns)]
                        if let Ok(path) = PathBuf::from_str(item.as_str()) {
                            if path.exists() && crawler::check_file_type(&path) {
                                files.push(path);
                            }
                        }
                    }

                    if files.len() > 0 {
                        add_dialog_controller::creat_dialog_for_files_drop(&files, &mut con_clone, &s, TransferMode::Add,true)
                    }
                    
                    
                    dnd = false;
                    released = false;
                    true
                } else {
                    false
                }
            }
            Event::DndLeave => {
                dnd = false;
            released = false;
                true
            }
            _ => false,
        }
    });

    let mut filter_input_option: Option<fltk::input::Input> = None;

    while app.wait() {

        if let Some(msg) = r.recv() {
            match msg {
                Message::Filter => filter_input_option = filter_controller::show_dialog(&mut controlls, &s),
                Message::Add => add_dialog_controller::show_dialog(&mut controlls, &s, TransferMode::Add),
                Message::Move => move_copy_selected(&mut controlls, &s),
                Message::Rename => rename_selected(&mut controlls, &s),
                Message::Delete => delete_selected(&mut controlls), 
                Message::Export => export_selected(&mut controlls),
                Message::Refresh => controlls.update_model(),
                Message::CloseWindow => app.quit(),
                Message::Resize(size_x, size_y) => controlls.resize(size_x, size_y),
                
                //Messaging
                Message::SetSort(sort) => { controlls.model.sort = sort; controlls.update_model(); },
                Message::SetTextFilter => {
                    if let Some(text_input) = &filter_input_option {
                        controlls.model.text_filter = text_input.value().to_owned().trim().to_string();
                        controlls.update_model();
                    }
                },
                Message::ResetFilter => { controlls.model.clear_filter(); controlls.update_model(); },
            }
        }

        make_it_select_rows(&mut controlls);
    }
}

fn rename_selected(controlls: &mut Controlls, s: &Sender<Message>) {
    let list = get_selected_setups(controlls);

    if list.len() != 1 {
        return;
    }

    if let Some(item) = list.get(0) {
        if let Some(new_name) = dialog::input_default("Rename", &item.name) {
            let res = crawler::check_for_file(&controlls.model.acc_settings_folder, &item.car_name, &item.track, &new_name);

            if res.is_err() {
                //We need to ask the user before we overwrite the file
                let val = dialog::choice2_default(format!("A Setup with the name {} already exists. Do you want to continue renaming Overwrite the other file?", &new_name).as_str(), "Cancel", "Edit", "Overwrite");

                match val {
                    Some(2) => (), //proceed
                    Some(1) => {s.send(Message::Rename); return;}, //edit
                    Some(0) => return, //exit
                    None => {s.send(Message::Rename); return;},
                    _ => {s.send(Message::Rename); return;} //edge case, doing same es edit
                }
            }

            let dest = match res {
                Ok(k) => k,
                Err(e) => e
            };

            if let Err(err) = fs::rename(&item.path, dest) {
                dialog::alert_default(format!("Failed to rename setup: {}", err).as_str());
            }
            s.send(Message::Refresh);
        }
    }

}

fn move_copy_selected(controlls: &mut Controlls, s: &Sender<Message>) {
    let list = get_selected_setups(controlls);

    let mut files: Vec<PathBuf> = Vec::with_capacity(list.len());

    for item in list {
        files.push(item.path.clone().into_path_buf());
    }

    add_dialog_controller::creat_dialog_for_files(&files, controlls, s, TransferMode::Copy);
}

fn delete_selected(controlls: &mut Controlls) {
    let list = get_selected_setups(controlls);

    if list.len() == 0 {
        return;
    }

    let message: String = if list.len() == 1 {
        format!("Are you sure you want to delete {}?", list.get(0).unwrap().name)
    } else {
        format!("Are you sure you want to delete these {} Setups?", list.len())
    };

    if let Some(decision) = dialog::choice2_default(message.as_str(), "Cancel", "Confirm", "") {
        if decision == 1 {
            //Going ahead and deleting the setups
        
            for item in list {
                if let Err(err) = fs::remove_file(&item.path) {
                    dialog::alert_default(format!("Error occured when trying to delete {}, {}.\nMoving on", &item.name, err.to_string()).as_str())
                }
            }

            controlls.update_model();
        }
    }
}

fn export_selected(controlls: &mut Controlls) {
    let setups = get_selected_setups(controlls);

    let mut dialog = FileDialog::new(FileDialogType::BrowseDir);
    dialog.set_option(FileDialogOptions::NewFolder);

    if let Some(desk) = dirs::desktop_dir() {
        dialog.set_directory(&desk).unwrap();
    }

    
    dialog.show();

    let folder = dialog.filename();

    for item in setups {
        let mut target = folder.clone();
        let original = PathBuf::from(item.path.clone());

        target.push(original.file_name().unwrap());

        if target.exists() {
            //File exists
            let res = dialog::choice2_default(format!("File {}.json already exists, do you want to overwrite it?", item.name.as_str()).as_str(), "Cancel", "Skip", "Overwrite");

            match res {
                Some(0) => return,
                Some(1) => target.clear(),
                Some(2) => (),
                None => return,
                _ => target.clear()
            }
        }

        if target.is_absolute() {
            if let Err(err) = fs::copy(original, target) {
                dialog::alert_default(format!("Error when copying file: {}", err).as_str());
            }
        }
    }
}

fn get_selected_setups(controlls: &mut Controlls) -> Vec<Setup> {
    let (row_top, _col_left, row_bottom, _col_right) = controlls.setup_table.get_selection();

    let mut list: Vec<Setup> = Vec::with_capacity((row_bottom - row_top + 1) as usize);
    
    if let Some(out) = controlls.model.view_list.get((row_top as usize) .. ((row_bottom + 1) as usize)) {
        for item in out {
            list.push(item.to_owned());
        }
    }

    list
}

fn make_it_select_rows(controlls: &mut Controlls) {
    let (row_top, col_left, row_bottom, col_right) = controlls.setup_table.get_selection();

    //Disables buttons that require something to be selected
    if (row_bottom - row_top) >= 0 && row_top >= 0 {
        //Renaming only when one row is selected
        if (row_bottom - row_top) > 0 && controlls.btn_rename.active() {
            controlls.btn_rename.deactivate()
        } else if (row_bottom - row_top) == 0 && !controlls.btn_rename.active() {
            controlls.btn_rename.activate()
        }

        if !controlls.btn_move.active() {
            controlls.btn_move.activate();
            controlls.btn_del.activate();
            controlls.btn_export.activate();
        }

    } else if controlls.btn_move.active() { //nothing selected
        controlls.btn_move.deactivate();
        controlls.btn_rename.deactivate();
        controlls.btn_del.deactivate();
        controlls.btn_export.deactivate();
    }

    if col_left == 0 && col_right == 3 {
        return;
    }

    controlls.setup_table.set_selection(row_top, 0, row_bottom, 3);
}
