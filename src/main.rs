#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

pub mod view;
pub mod model;
pub mod controller;
pub mod backend;

use fltk::{app, dialog, enums::FrameType};




fn main() {
    let app = app::App::default();

    app::set_frame_type(FrameType::FlatBox);

    let res = match backend::crawler::get_acc_folder() {
        Ok(res) => res,
        Err(res) => {
            dialog::beep(dialog::BeepType::Error);
            dialog::alert_default("$STEAM_DIR was set, but invalid. Continuing...");
            res
        }
    };

    let acc_settings_folder = if let Some(model) = res {
        model.into_boxed_path()
    } else {
        dialog::beep(dialog::BeepType::Error);
        dialog::alert_default("Documents/Assetto Corsa Competizione folder not found.\n
            Make sure you installed the game and ran the game at least once");
        return;
    };
    

    let model = model::main_model::MainModel::new(acc_settings_folder);
    

    let controlls = view::main_view::create_window(model);
    
    
    
    controller::main_controller::run_app(app, controlls);
}

