# ACC-SetupManager
A simple, lightweight, crossplatform Setup Manager for Assetto Corsa Competizione.  
Purpose is to allow moving setups around through a simple UI rather then crawling through folders, speeding up distributing setups.  

## Distribution
### Windows Download:
*wip*  
  
### Linux:
AUR: [acc-setupmanager-git](https://aur.archlinux.org/packages/acc-setupmanager-git)
  
## Features  
Currently available:
- Sorting and searching
- Importing setup.json's through drag and drop or menu
- Exporting singular or bulk through the menu
- Moving, Deleting and Renaming Setups
- Windows and Linux (Wayland & X11) support

Planned to arrive... *eventually*:
- Preview of vital settings (Pressures, compound, fuel) of a selected setup  
- Handling of zip files (for both import and export)  
- Editing Setups  

## Build Instructions
Requires [Rust](https://www.rust-lang.org/) installed.  
  
To build the release version use this command:  
`cargo build --release`  
  
For Linux what you need installed depends on the features you use:  
- `use-wayland` requires `wayland` and `x11` related lirbaries
- `dynamic` requires `fltk` (v1.4+) installed and just `libx11` (and just `wayland` if used with `use-wayland`)
- `bundled` uses a precompiled version of fltk and statically links it, only x11 support
- Using no features (or just `use-wayland`) will compile fltk from source and statically link it, requiring all libraries for it
For a list of libraries (that you probably have installed already) check out on the [fltk-rs](https://github.com/fltk-rs/fltk-rs?tab=readme-ov-file#build-dependencies) crate directly.
